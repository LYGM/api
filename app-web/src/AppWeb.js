import { html, css, LitElement } from 'lit';

export class AppWeb extends LitElement {
    static get styles() {
        return css `
      :host {
        display: block;
        padding: 25px;
        color: var(--app-web-text-color, #000);
      }
    `;
    }

    static get properties() {
        return {
            datos: { type: Array },

        };
    }

    constructor() {
        super();
        this.datos = [];
    }

    firstUpdated() {
        fetch('https://api.datos.gob.mx/v2/Releases_SFP')
            .then((respons) => respons.json())
            .then((respons) => {
                console.log(respons);
                this.datos = respons;
                //console.log(this.datos);
                //console.log(this.datos.results[4]._id);
                let id = '';
                this.datos.results.forEach(ids => {

                    id = id + `<table><tr>
                    <th>${ids._id}</th>
                    <th>${ids.publisher.uid}</th>
                    <th>${ids.publisher.name}</th>
                    <th>${ids.publisher.uri}</th>
                    <th>${ids.cycle}</th>
                    <th>${ids.ocid}</th>
                    <th>${ids.id}</th>
                    <th>${ids.id}</th>
                    <th>${ids.date}</th>
                    <th>${ids.tag}</th>
                    <th>${ids.initiationType}</th>
                    </tr></table><br>`

                });
                document.getElementById('container').innerHTML = id;

            });

    }

    render() {
        return html `
            <h1>Datos</h1>
            
    `;
    }
}